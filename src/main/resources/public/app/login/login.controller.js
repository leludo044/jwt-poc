app.controller('LoginCtrl',
		[
				'$scope',
				'$http',
				'$location',
				'$localStorage',
				'authenticationService',
				function($scope, $http, $location, $localStorage,
						authenticationService) {

					var self = this;

					this.dataLoading = false;
					this.error = undefined;
					this.username = "test";
					this.password = "test";

					authenticationService.logout() ;	

					this.login = function() {

						authenticationService.login(this.username, this.password,
								function(isAuthenticated) {
									if (isAuthenticated === true) {
										$location.path('/');
									} else {
										self.error = "authentication failed !";
									}
								});
					}
				} ]);
