'use strict';

/**
 * 
 */
var app = angular.module('App', [ 'ngRoute', 'ngStorage']);

/**
 * Route configuration
 */
app.config(function($routeProvider) {

	$routeProvider.when('/', {
		templateUrl : 'app/home/home.html',
		controller : 'HomeCtrl',
		controllerAs : 'vm',
	}).when('/login', {
		templateUrl : 'app/login/login.html',
		controller : 'LoginCtrl',
		controllerAs : 'ctrl'
	}).otherwise({
		redirectTo : '/'
	});
});

/**
 * Execution block at startup or after page refresh.
 */
app.run(run);

function run($rootScope, $http, $location, $localStorage) {
	console.log("running...");

	// Check for an existing token
	if ($localStorage.currentUser) {
		// Attach it by default to each request
		$http.defaults.headers.common.Authorization = 'Bearer '
				+ $localStorage.currentUser.token;
		console.log("token found for " + $localStorage.currentUser.username);
	} else {
		console.log("token not found !");
	}

	// Register for change location
	$rootScope.$on('$routeChangeStart', function(event) {
		console.log('location changed to ' + $location.path());
		if (!$localStorage.currentUser) {
			$location.path("/login")
		}
	});
};
