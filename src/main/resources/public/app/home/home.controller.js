app.controller('HomeCtrl', ['$scope', '$http', '$location', '$localStorage', 'authenticationService',
	function ($scope, $http, $location, $localStorage, authenticationService) {

		var self = this;

		this.profil = "dont know";
		this.token = "" ;

		/*
		 * Check for token to display 
		 */
		if ($localStorage.currentUser) {
			this.token = $localStorage.currentUser.token;
		}
		
		/*
		 * Ask status from server
		 */
		$http.get('api/status').success(function (response) {
			self.profil = response;
		})

		/*
		 * Logout action on "Logout" button click
		 */
		this.logout = function () {
			authenticationService.logout() ;
		}

	}]);
