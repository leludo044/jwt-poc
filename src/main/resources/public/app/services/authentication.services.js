app.factory("authenticationService", authenticationService);
function authenticationService($http, $localStorage, $location) {
    var service = {};
    service.login = login;
    service.logout = logout;
    return service;

    function login(username, password, callback) {
        $http.post('api/authenticate', {
            username: username,
            password: password
        }).success(
            function (response) {
                if (response.error) {
                    self.error = response.error;
                    callback(false)
                } else if (response.id) {
                    self.tokenId = response.id;
                    $localStorage.$default({currentUser : {
                        username: username,
                        token: response.id
                    }});
                    $http.defaults.headers.common.Authorization = 'Bearer '
                        + response.id;
                    callback(true);
                }
            })
    };

    function logout() {
        // remove user from local storage and clear http auth header
        delete $localStorage.currentUser;
        $http.defaults.headers.common.Authorization = '';
        // Return to login page
		$location.path('/login') ;
    };
}