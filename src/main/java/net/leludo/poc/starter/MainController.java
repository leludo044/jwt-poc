package net.leludo.poc.starter;

import java.security.Key;
import java.util.Hashtable;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import net.leludo.poc.starter.security.Credentials;
import net.leludo.poc.starter.security.Token;

@Controller
@EnableAutoConfiguration
@RequestMapping("/api")
public class MainController
{

    Key key;

    @RequestMapping(path = "/status", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    String home(@RequestHeader("Authorization") String token)
    {
        String status = "not admin";
        if (!token.equals(""))
        {
            String compactJws = token.substring(7);
            Map<String, Object> claims = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(compactJws).getBody();
            if (claims.get("profil").equals("admin"))
            {
                status = "admin";
            }
        }
        return status;
    }

    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    @ResponseBody
    public Token authenticate(@RequestBody Credentials credentials)
    {
        Token token = new Token();
        if (credentials.getPassword().equals("test"))
        {
            Map<String, Object> claims = new Hashtable<String, Object>();
            claims.put("profil", "admin");

            key = MacProvider.generateKey();
            System.out.println(key.toString());

            String s = Jwts.builder().setSubject("Ludovic").setClaims(claims)
                    .signWith(SignatureAlgorithm.HS512, key).compact();
            token.setId(s);
        } else {
            token.setError("Bad credentials ! Try again.");
        }
        return token;
    }

    @RequestMapping(path = "/isadmin", produces = MediaType.TEXT_PLAIN_VALUE)
    String isAdmin()
    {
        return "is not admin !";
    }

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    @ResponseBody
    public Token authenticate2()
    {
        Key key = MacProvider.generateKey();
        System.out.println(key.toString());

        String s = Jwts.builder().setSubject("Ludovic")
                .signWith(SignatureAlgorithm.HS512, key).compact();

        return new Token();
    }

    public static void main(String[] args)
    {
        SpringApplication.run(MainController.class, args);
    }

    String getServerPort()
    {
        return "8081";
    }
}
