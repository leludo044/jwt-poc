package net.leludo.poc.starter.security;

public class Credentials
{
    private String username;
    private String password;

    /**
     * @return the username
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }

}
