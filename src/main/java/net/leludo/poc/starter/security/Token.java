package net.leludo.poc.starter.security;

import java.io.Serializable;

public class Token implements Serializable
{
    private String id;
    private String error;

    public Token()
    {
        this.id = "";
        this.error = "";
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return the error
     */
    public String getError()
    {
        return error;
    }

    /**
     * @param error
     *            the error to set
     */
    public void setError(String error)
    {
        this.error = error;
    }

}
